import java.util.ArrayList;

public class Dealership {
	private ArrayList<Employee> employees;
	private ArrayList<Product> products;
	private String name;
	
	public Dealership(){
		this.employees = new ArrayList<Employee>();
		this.products = new ArrayList<Product>();
	}
	
	@Override
	public String toString(){
		
		return this.getName();		
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj == null) return false;
		if(obj == this) return true;
		Dealership dealership = (Dealership) obj;
		if(this.getName().compareTo(dealership.getName()) != 0){
			return false;
		}
		return true;
	}
	
	public boolean Sell(Product product,Client client){
		return false;
	}
	public ArrayList<Employee> getEmployees() {
		return employees;
	}
	public void setEmployees(ArrayList<Employee> employees) {
		this.employees = employees;
	}
	public ArrayList<Product> getProducts() {
		return products;
	}
	public void setProducts(ArrayList<Product> products) {
		this.products = products;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
}
