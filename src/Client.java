
public class Client {
	private String name;
	private String product;
	
	public Client(){
		this.name="";
		this.product="";
	}
	
	@Override
	public String toString(){

		return this.getName();		
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj == null) return false;
		if(obj == this) return true;
		Client client = (Client) obj;
		if(this.name.compareTo(client.getName()) != 0){
			return false;
		}
		return true;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}	
}

