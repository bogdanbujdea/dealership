import java.util.ArrayList;


public class Employee {
	private String name;
	private double salary;
	private ArrayList<Product> soldProducts;
	
	public Employee(){
		this.name="";
		this.salary=0;
	}
		
	@Override
	public String toString(){
		
		return this.getName() + " has a salary of " + this.getSalary();		
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj == null) return false;
		if(obj == this) return true;
		Employee employee = (Employee) obj;
		if(this.name.compareTo(employee.getName()) != 0){
			return false;
		}
		return true;
	}
	
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}

	public ArrayList<Product> getSoldProducts() {
		return soldProducts;
	}

	public void setSoldProducts(ArrayList<Product> soldProducts) {
		this.soldProducts = soldProducts;
	}
	
}
