import java.util.ArrayList;


public class MainProgram {

	public static void main(String[] args) {

		Dealership dealership = new Dealership();
		dealership.setName("Audi");
		addEmployees(dealership);
		printEmployees(dealership.getEmployees());
		addProducts(dealership);
		printProducts(dealership.getProducts());
		Car car = new Car();
		car.setYear(1822);
	}

	private static void addProducts(Dealership dealership) {
		Car car = new Car();
		car.setColor("red");
		car.setName("Audi Q7");
		car.setModel("Q7");
		car.setPrice(40000);
		car.setYear(1999);
		car.setManufacturer("Audi");
		Service service = new Service();
		service.setName("Car fix");
		service.setPrice(300);
		dealership.getProducts().add(car);
		dealership.getProducts().add(service);
	}

	private static void printProducts(ArrayList<Product> products) {
		
		for (Product product : products) {
			System.out.println(product);
		}
	}

	private static void addEmployees(Dealership dealership) {
		ArrayList<Employee> dealers = new ArrayList<Employee>();
		ArrayList<Product> products = new ArrayList<Product>();
		products.add(new Car(14000, "blue"));
		dealers.add(new Dealer("Ion", 1000.0, products));
		products = new ArrayList<Product>();
		products.add(new Service(140, "Car paint"));
		products.add(new Car(35000, "pink"));
		dealers.add(new Dealer("Ghita", 1200, products));
		products = new ArrayList<Product>();
		products.add(new Service(20, "Car wash"));
		products.add(new Car(16000, "black"));
		dealers.add(new Dealer("Paul", 1500, products));
		dealership.setEmployees(dealers);
	}

	private static void printEmployees(ArrayList<Employee> employees) {
		for (Employee employee : employees) {
			System.out.println(employee);
		}
	}

}
