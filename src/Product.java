
public abstract class Product {
	private String name;
	private double price;

	@Override
	public abstract String toString();
	
	@Override
	public abstract boolean equals(Object obj);
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public abstract double getDealerBonus();
	
}