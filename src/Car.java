
public class Car extends Product {
	private String manufacturer;
	private int year;
	private String color;
	private String model;
	private double dealerPercentage;
	
	public Car(){
		this.manufacturer = "";
		this.year = 0;
		this.color = "";
		this.model = "";
		dealerPercentage = 0.01;
	}
	
	public Car(double price, String color){
		this.manufacturer = "";
		this.year = 0;
		this.color = "";
		this.model = "";
		this.setPrice(price);
		dealerPercentage = 0.01;
	}
	
	@Override
	public String toString(){
		
		return this.getManufacturer() + " " + this.getModel();		
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj == null) return false;
		if(obj == this) return true;
		Car car = (Car) obj;
		if(this.getName().compareTo(car.getName()) != 0){
			return false;
		}
		return true;
	}
	
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		
		if(year < 1900 || year > 2014){
			System.out.println("The year is not valid");
		}
		else{
			this.year = year;			
		}
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	@Override
	public double getDealerBonus() {
				
		return this.getPrice() * dealerPercentage;
	}
}
