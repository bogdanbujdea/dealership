import java.util.ArrayList;

public class Dealer extends Employee{
	
	
	public Dealer(){
	}
	

	public Dealer(String name, double salary, ArrayList<Product> soldProducts){
		this.setName(name);
		this.setSalary(salary);
		this.setSoldProducts(soldProducts);
	}
	
	@Override
	public String toString(){
		
		return this.getName() + " has a salary of " + this.getSalary() + " and a bonus of " + this.getBonus();		
	}

	@Override
	public boolean equals(Object obj){
		if(obj == null) return false;
		if(obj == this) return true;
		Dealer dealer = (Dealer) obj;
		if(this.getName().compareTo(dealer.getName()) != 0){
			return false;
		}
		return true;
	}
	
	public double getBonus() {
		double sum = 0;
		for (Product product : this.getSoldProducts()) {
			sum += product.getDealerBonus();
		}
		return sum;
	}
	
}
