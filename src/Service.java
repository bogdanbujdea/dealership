
public class Service extends Product {
	private String serviceType;
	private double dealerPercentage;

	public Service(){
		this.serviceType = "";
		dealerPercentage = 0.02;
	}
	
	public Service(double price, String serviceType){
		this.serviceType = serviceType;
		this.setPrice(price);
		dealerPercentage = 0.02;
	}
	
	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	
	@Override
	public String toString(){
		
		return this.getName() + " " + this.getServiceType();		
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj == null) return false;
		if(obj == this) return true;
		Service service = (Service) obj;
		if(this.getName().compareTo(service.getName()) != 0){
			return false;
		}
		return true;
	}

	@Override
	public double getDealerBonus() {
		
		return this.getPrice() * dealerPercentage;
	}
}
